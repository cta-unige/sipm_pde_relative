import numpy as np
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join
import math

from scipy.misc import electrocardiogram
from scipy.signal import find_peaks
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.interpolate import interp1d

class IV:

    def __init__(self, file_name):

        '''

        This class is developed to read IV data: forward and reverse

        :param file_name: file with measurements. File struucture should be like:
            Voltage(V)	Current(A)	DiodeCurrent(A)	DiodeCurrent Std(A)
            0.0000E+0	2.6310E-12	-1.7556E-12	562.3528E-15
            2.0000E+0	46.7964E-12	-1.3972E-12	522.7542E-15
            4.0000E+0	75.4595E-12	-1.5427E-12	231.4481E-15
            6.0000E+0	199.4555E-12	-1.7871E-12	1.0581E-12
            8.0000E+0	263.4487E-12	-1.4118E-12	56.7878E-15
            10.0000E+0	306.6299E-12	-1.3389E-12	134.4486E-15
        '''

        self.n_data = 0
        self.voltage = []
        self.current = []
        self.pd_current = []
        self.pd_std = []
        self.file = file_name
        self.vbd = 0
        self.wavelength = 0
        #self.figure = plt.figure(figsize=(12, 8))


    def read_IV_file(self):

        '''
        read IV from txt file
        return Voltage, Current and photodiode current and std
        '''

        voltage_tmp = []
        current_tmp = []
        pd_current_tmp = []
        pd_std_tmp = []

        #print('start read IV data')

        with open(self.file, "r") as data_file:
            # print(file_name)
            lines = data_file.readlines()
            for line in lines:
                if (line.find('Voltage(V)') == -1):
                    words = line.split()
                    voltage_tmp.append(float(words[0]))
                    current_tmp.append(float(words[1]))

                    pd_current_tmp.append(float(words[2]))
                    pd_std_tmp.append(float(words[3]))
        self.voltage = voltage_tmp
        self.current = current_tmp
        self.pd_current = pd_current_tmp
        self.pd_std = pd_std_tmp

    def get_current(self, overvoltage):

        '''
        get SiPM current at a given Overvoltage
        :param overvoltage:
        :return:
        '''

        if self.vbd == 0.:
            self.calc_vbd()

        try:
            print(self.iv_iterpolate(overvoltage+self.vbd))
        except:
            self.iv_iterpolate = interp1d(self.voltage, self.current, kind='cubic')
            print(self.iv_iterpolate(overvoltage + self.vbd))


    def draw_iv(self, ax, plot_label, do_log = True, do_vbd = False):

        '''

        :param ax: - ax to draw IV
        :param plot_label: plot label to be used
        :param do_log: do log Y scale
        :param do_vbd: calculate or not breakdown voltage
        :return: image of the IV curve
        '''

        #ax.figure(figsize=(12,8))

        try:
            x = np.arange(0.0, 60., 0.1)
            ax.plot(x, self.iv_iterpolate(x), '-', label = plot_label)
        except:
            print('no interpolation were done')

        ax.plot(self.voltage, self.current, '.--', label = plot_label)
        ax.grid(True)
        if do_log:
            #plt.yscale('log')
            ax.set_yscale('log')
            ax.set_xlabel('Voltage, V')
            ax.set_ylabel('Current, A')

        if do_vbd:
            ax.plot([self.vbd, self.vbd], [1.e-12, 1.e-2], '--r')
        ax.legend()
        #plt.show()

    def gauss_function(self, x, a, x0, sigma):

        '''
        Gaussian fit function
        :param x: - data
        :param a: - amplitude
        :param x0: - mean
        :param sigma: - STD
        :return: fit parameters
        '''

        return a * np.exp(-(x - x0) ** 2 / (2 * sigma ** 2))

    def get_wavelength(self):

        '''
        get wavelength from file name
        :return: wavelength
        '''

        wl_start = self.file.find('WL_') + 3
        wl_end = self.file.find('_nm')

        self.wavelength = int(self.file[wl_start:wl_end])

    def calc_vbd(self, dv = 0.01, debug = False):
        '''
        lets calculate Vbd from invert second derivative method
        '''

        self.iv_iterpolate = interp1d(self.voltage, self.current, kind='cubic')

        x_interpolate = np.arange(self.voltage[0], self.voltage[-1], dv)

        self.dvoltage = np.gradient( np.log(self.iv_iterpolate(x_interpolate)))
        self.d2voltage = np.gradient(self.dvoltage)

        peak_arg = np.argmax(self.d2voltage)
        x_fit = x_interpolate[peak_arg - 10:peak_arg + 10]
        y_fit = self.d2voltage[peak_arg - 10:peak_arg + 10]

        popt_vbd, pcov_vbd = curve_fit(self.gauss_function, x_fit, y_fit, p0=[self.d2voltage[peak_arg], x_interpolate[peak_arg], 0.01], maxfev=10000)

        if debug:
            print('found Vbd = ', popt_vbd[1])

            #plt.figure(figsize=(12, 8))
            plt.plot(x_interpolate, self.d2voltage, '.--')
            plt.plot(x_fit, self.gauss_function(x_fit, *popt_vbd), 'b--')
            plt.plot(popt_vbd[1], popt_vbd[0], 'or')
            plt.grid()
            plt.xlabel('Voltage, V')
            plt.ylabel('Current, I')
            plt.show()

        self.vbd = popt_vbd[1]

