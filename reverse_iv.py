import numpy as np
from os import listdir
from os.path import isfile, join
import math

from scipy.misc import electrocardiogram
from scipy.signal import find_peaks
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.interpolate import interp1d
from IV import IV
import matplotlib.pyplot as plt
import pandas as pd


class relative_pde(IV):
    '''
    this class should be used for relatve PDE calculation
    '''

    n_data = 0


    def __init__(self, file_path, wl_err, qe_err = 5):

        '''

        :param file_path: path to the folder with IV data
        :param wl_err:    monochromator wavelength resolution
            for our monochromator is a function of slit width:
                slit (mm)    wavelength resolution (nm):
                0.05    0.7
                0.120   1.6
                0.280   3.7 (3.6 for wavelength > 690 nm)
                0.600   8.0 (7.8 for wavelength > 690 nm)
                0.760   10.1 (9.9 for wavelength > 690 nm)
                1.240   16.5 (16.1 for wavelength > 690 nm)
                1.560   20.6 (20.3 for wavelength > 690 nm)
                3.160   42.1 (41.1 for wavelength > 690 nm)


        :param qe_err:    photodide calibration presision, by defolt for our Hamamatsu S1337-1010BQ is 5%
        '''

        
        self.qe_err = qe_err    # Calibrated QE error from producer
        self.wl_err = wl_err    # Wavelength resolution
        self.path = file_path   # Path to folder with measurements data
        self.get_files()        # All files at folder:  file_path
        self.sipm_data ={}
        self.vbd_dict = {}
        self.photodiode_data = {}
        self.photodiode_data_std = {}
        self.wavelength_data = []
        self.device_name = ''
        self.photocurent = {}
        self.sipm_photocurent = {}
        self.photodiode_photocurent = {}
        self.photodiode_photocurent_std = {}
        self.pde_relative = {}
        self.pde_relative_err = {}
        self.fit_absolute_pde = {}
        self.pde_absolute = {}
        self.pde_absolute_wl_err = {}
        self.pde_ziped = {}
        self.pde_ziped_err = {}
        self.pde_ziped_wl_err = {}

    def pde_absolute_fit(self, x, const, slope):
        '''

        Used to Fit absolute PDE data, or get absolute PDE at a given Overvoltage from fit parameters

        :param x: - overvoltage
        :param const: - Max PDE value
        :param slope: - PDE slop
        :return: PDE fit
        '''
        return const*(1. - np.exp(-x*slope))
    
    def get_qe(self, path_qe):

        '''

        :param path_qe: file with QE data for calibrated Photodiode
        :return: get QE at a given wavelength and interpolation function "f_qe"
        '''
        
        self.path_qe = path_qe
        qe_data = np.loadtxt(self.path_qe, dtype=float, comments='#', delimiter = ' ')
        self.wl_qe = qe_data[:,0]
        self.qe = qe_data[:,1]
        self.f_qe = interp1d(self.wl_qe, self.qe, kind='cubic')

    def calc_norm_factor(self):

        '''

        :return: calculate the normalization factor for relative PDE from absolute
        '''

        wl_absolute = list(self.pde_absolute.keys())[1:]
        print("wavelengths   : ",wl_absolute)
        self.k_factor = np.mean(list(self.pde_absolute.values()))/np.mean(self.f_pde_relative_zipped(wl_absolute))
        print("mean absolute : ", np.mean(list(self.pde_absolute.values())) )
        print("mean relative : ", np.mean(self.f_pde_relative_zipped(wl_absolute)) )
        print("norm factor   : ", self.k_factor )

    def get_absoluter_pde(self, df_file, overvoltage):

        '''

        :param df_file: - exel file with absolute PDE fit parameters
        :param overvoltage: - overvoltage at which we want to have absolute PDE values
        :return: normalization factor for relative PDE to get Absolute PDE vs. wavelength
        '''

        self.read_absolute_pde(df_file)
        for wl_key in self.fit_absolute_pde.keys():
            const = self.fit_absolute_pde[wl_key][0]
            slope = self.fit_absolute_pde[wl_key][1]
            self.pde_absolute[wl_key] = self.pde_absolute_fit(overvoltage, const, slope)
            self.pde_absolute_wl_err[wl_key] = self.fit_absolute_pde[wl_key][2]
            print("key : ", wl_key, ' pde : ', self.pde_absolute[wl_key])

        print('mean AC pde : ', np.mean(list(self.pde_absolute.values())) )
        self.calc_norm_factor()

    def read_absolute_pde(self, df_file):

        '''

        :param df_file: exel file with absolute PDE fit parameters
        :return: plot absolute PDE from its fit parameters
        '''

        self.pde_df = pd.read_csv(df_file)
        print(self.pde_df.head())
        overvoltage = np.arange(0, 10, .1)

        plt.figure(8)
        for i in range(len(self.pde_df['wavelength (nm)'])):
            wavelength_nm = self.pde_df['wavelength (nm)'][i]
            pad_absolute = self.pde_absolute_fit(overvoltage, self.pde_df['PDE Max.'][i], self.pde_df['PDE Slop'][i])
            plt.plot(overvoltage, pad_absolute, '.-', label=wavelength_nm)
            self.fit_absolute_pde[wavelength_nm] = [self.pde_df['PDE Max.'][i], self.pde_df['PDE Slop'][i], self.pde_df['wavelength err. (nm)'][i]]

        plt.grid()
        plt.legend()
        plt.ylim([0., 70.])
        plt.xlim([0., 10.])
        plt.savefig('results/pde_absolute.png')

    def calc_relative_pde(self, file_ratio, file_qe, dV, Vbd = np.NAN, debug=False):

        '''

        Used to calculate the relative PDE

        :param file_ratio: file with photocurent from two photodiodes, one located on integration sphere another replace the SiPM
        :param file_qe: wile with calibrated Photodiode QE, from producer
        :param dV: Overvoltage at which analisis should be done
        :param Vbd: Breakdown voltage, if not defined (np.NAN) the Vbd will be calculated for each IV from reverse second derivative method;
                    if defined (single value), will be used for all IVs;
        :param debug: if we need to show intermideat results
        :return:
        '''
        
        self.get_qe(file_qe)
        self.read_ratio_file(file_ratio)
        self.get_iv_dictionary(Vbd, debug)
        self.get_photocurent(dV, debug=debug)

        for wl_key in self.sipm_photocurent.keys():

            if self.f_ratio(500) >= 1.:
                self.pde_relative[wl_key] = self.f_ratio(int(wl_key))*self.f_qe(int(wl_key))*self.sipm_photocurent[wl_key]/self.photodiode_photocurent[wl_key]
            else:
                self.pde_relative[wl_key] = self.f_qe(int(wl_key)) * self.sipm_photocurent[
                    wl_key] / (self.photodiode_photocurent[wl_key]*self.f_ratio(int(wl_key)) )
                
            self.pde_relative_err[wl_key] = math.sqrt( self.pde_relative[wl_key]*self.pde_relative[wl_key]*0.01*self.qe_err*0.01*self.qe_err +
                                                       self.pde_relative[wl_key]*self.pde_relative[wl_key]*self.photodiode_photocurent_std[wl_key]*self.photodiode_photocurent_std[wl_key]/(self.photodiode_photocurent[wl_key]*self.photodiode_photocurent[wl_key]) )

            #self.pde_relative_err[wl_key] = math.sqrt(self.pde_relative[wl_key] * self.pde_relative[wl_key] * 0.01*self.qe_err * 0.01*self.qe_err)

        max_pde = np.max(list(self.pde_relative.values()))
        
        for wl_key in self.sipm_photocurent.keys():
            self.pde_relative[wl_key] = self.pde_relative[wl_key]/max_pde
            self.pde_relative_err[wl_key] = self.pde_relative_err[wl_key]/max_pde
            



    def zip_pde_relative(self, data_run_1, data_run_2, zip_key):

        '''
        Function to Zip data from different measuremetns (i.e. different slit width)
        :param data_first: object of relative_pde calas with calculated relative PDE
        :param data_second: object of relative_pde calas with calculated relative PDE
        :param zip_key: wavelength at which we should zip data (should be at both objects)
        :return:
            pde_ziped - zipped relative PDE
            pde_ziped_err - zipped relative PDE errors
            f_pde_relative_zipped - interpolation of zipped PDE
        '''

        wl_mean_run1 = np.mean(list(data_run_1.pde_relative.keys()))
        wl_mean_run2 = np.mean(list(data_run_2.pde_relative.keys()))

        if wl_mean_run1 <= wl_mean_run2:
            dict_first = data_run_1.pde_relative
            dict_first_err = data_run_1.pde_relative_err
            dict_first_wl_err = data_run_1.wl_err

            dict_second = data_run_2.pde_relative
            dict_second_err = data_run_2.pde_relative_err
            dict_second_wl_err = data_run_2.wl_err
        else:
            dict_first = data_run_2.pde_relative
            dict_first_err = data_run_2.pde_relative_err
            dict_first_wl_err = data_run_2.wl_err

            dict_second = data_run_1.pde_relative
            dict_second_err = data_run_1.pde_relative_err
            dict_second_wl_err = data_run_1.wl_err

        k1 = dict_first[zip_key]
        k2 = dict_second[zip_key]

        for wl_key in dict_first.keys():
            if wl_key < zip_key:
                self.pde_ziped[wl_key] = k2*dict_first[wl_key]/k1
                self.pde_ziped_err[wl_key] = k2 * dict_first_err[wl_key] / k1
                self.pde_ziped_wl_err[wl_key] = dict_first_wl_err
            else:
                self.pde_ziped[wl_key] = k1*dict_second[wl_key]/k2
                self.pde_ziped_err[wl_key] = k1 * dict_second_err[wl_key] / k2
                self.pde_ziped_wl_err[wl_key] = dict_second_wl_err

        for wl_key in dict_second.keys():
            if wl_key < zip_key:
                self.pde_ziped[wl_key] = k2*dict_first[wl_key]/k1
                self.pde_ziped_err[wl_key] = k2 * dict_first_err[wl_key] / k1
                self.pde_ziped_wl_err[wl_key] = dict_first_wl_err
            else:
                self.pde_ziped[wl_key] = k1*dict_second[wl_key]/k2
                self.pde_ziped_err[wl_key] = k1 * dict_second_err[wl_key] / k2
                self.pde_ziped_wl_err[wl_key] = dict_second_wl_err

        pde_zipped_max = np.max(list(self.pde_ziped.values()))
        for wl_key in self.pde_ziped.keys():
            self.pde_ziped[wl_key] = self.pde_ziped[wl_key]/pde_zipped_max
            self.pde_ziped_err[wl_key] = self.pde_ziped_err[wl_key] / pde_zipped_max

        self.f_pde_relative_zipped = interp1d(list(self.pde_ziped.keys()), list(self.pde_ziped.values()), kind='cubic')



    def read_ratio_file(self, file_ratio):

        '''
        read wile with phototcurent measuremtns from two calibrated photodiodes. One on the top of integration sphere another is on SiPM position

        :param file_ratio: file with measuremtns
        :return: return interpolation function which represent the phtocurrent ratio for both photodiodes as a function of wavelength
        '''

        wavelength_ratio = []
        pd1_current_ratio = []
        pd1_std_ratio = []
        pd2_current_ratio = []
        pd2_std_ratio = []
        self.path_ratio = {}

        with open(file_ratio, "r") as data_file:

            lines = data_file.readlines()
            for line in lines:
                if (line.find('Voltage(V)') == -1):
                    words = line.split()
                    wavelength_ratio = float(words[0])
                    pd1_current_ratio = float(words[1])
                    pd1_std_ratio = float(words[2])
                    pd2_current_ratio = float(words[3])
                    pd2_std_ratio = float(words[4])

                    self.path_ratio[wavelength_ratio] = math.fabs(pd2_current_ratio/pd1_current_ratio)

        self.f_ratio = interp1d(list(self.path_ratio.keys()), list(self.path_ratio.values()), kind='cubic')

    #def get_ratio(self, file):



    def get_photocurent(self, overvoltage, debug = False):

        '''
        calculate the SiPM photocurrent at a given Overvoltage

        :param overvoltage: SiPM overvoltage
        :param debug: do debug or not
        :return: return SiPM photocurent and its interpolation
        '''

        self.photo_voltage = overvoltage

        for i_iv in range(len(self.wavelength_data)):

            wavelength_tmp = self.wavelength_data[i_iv]
            vbd_tmp = self.vbd_dict[wavelength_tmp]
            vbd_dark_tmp = self.vbd_dict['dark']

            vbias_tmp = vbd_tmp + overvoltage
            vbias_dark_tmp = vbd_dark_tmp + overvoltage

            photocurrent_tmp = self.sipm_data[wavelength_tmp](vbias_tmp)
            photocurrent_dark_tmp = self.sipm_data['dark'](vbias_dark_tmp)

            self.sipm_photocurent[wavelength_tmp] = photocurrent_tmp - photocurrent_dark_tmp
            self.photodiode_photocurent[wavelength_tmp] = math.fabs(self.photodiode_data[wavelength_tmp] - self.photodiode_data['dark'])
            self.photodiode_photocurent_std[wavelength_tmp] = math.sqrt(self.photodiode_data_std[wavelength_tmp]*self.photodiode_data_std[wavelength_tmp] + self.photodiode_data_std['dark']*self.photodiode_data_std['dark'])

            if debug:
                print("wavelength : ", wavelength_tmp, ' V bd : ', vbd_tmp, ' Vbias : ', vbias_tmp, " photocurent : ", photocurrent_tmp, ' photocurnet dark', photocurrent_dark_tmp, ' Vbias dark : ', vbias_dark_tmp)

        self.f_photodiode = interp1d(list(self.photodiode_photocurent.keys()),
                                     list(self.photodiode_photocurent.values()), kind='cubic')
        self.f_sipm = interp1d(list(self.sipm_photocurent.keys()),
                               list(self.sipm_photocurent.values()), kind='cubic')

        if debug:
            plt.figure(0)
            plt.plot(list(self.sipm_photocurent.keys()), list(self.sipm_photocurent.values()), '.', label = 'SiPM photrocurrent')
            plt.plot(list(self.photodiode_photocurent.keys()), list(self.photodiode_photocurent.values()), '.', label = 'Photodiode photrocurrent')
            plt.yscale('log')
            plt.ylim([1.e-10, 7.e-4])
            plt.grid()
            plt.legend()
            plt.savefig('results/photocurent.png')
                #plt.show()

    def do_vbd_distribution(self):

        '''
        plot Vbd distribution
        :return:
        '''

        min_vbd = np.min( list(self.vbd_dict.values()))
        max_vbd = np.max(list(self.vbd_dict.values()))

        plt.figure(1)
        plt.hist(list(self.vbd_dict.values()), bins = 50, range = [0.99*min_vbd, 1.01*max_vbd])
        plt.grid()
        plt.title(self.device_name)
        plt.xlabel("$V_{BD}$, V")
        plt.ylabel("n events")
        plt.savefig('results/V_BD.png')
        #plt.show()

    def get_files(self):

        '''
        get all files in a directory: self.path
        :return:
        '''

        onlyfiles = [f for f in listdir(self.path) if isfile(join(self.path, f))]
        self.files = onlyfiles
        self.files.sort()


    def get_iv_dictionary(self, Vbd, debug = False):

        '''
        Create dictionaris with SiPM and Photodiode currents. keys - wavelength, values are currents
        :param Vbd: breakdown voltage, if Vbd == np.NaN it will be calculated for each measurements
        :param debug: debug flag
        :return:
        '''

        #print('Vbd = ', Vbd)
        #plt.figure(2)
        for i_file in range(len(self.files)):
            if (self.files[i_file] != '.directory') and (self.files[i_file].find('dark') == -1):
                # print(iv_files[i_file])

                iv_data = IV(self.path + self.files[i_file])
                iv_data.get_wavelength()

                if debug:
                    print(iv_data.wavelength)

                wl_start = self.files[i_file].find('WL_') + 3
                wl_end = self.files[i_file].find('_nm')

                wavelength = int(self.files[i_file][wl_start:wl_end])
                self.wavelength_data.append(wavelength)

                if debug:
                    print('wl = ', wavelength, ' nm.')


                iv_data.read_IV_file()

                self.sipm_data[wavelength] = interp1d(iv_data.voltage, iv_data.current, kind='cubic')
                
                if np.isnan(Vbd):
                    iv_data.calc_vbd(dv = 0.01, debug = False)
                else:
                    iv_data.vbd = Vbd
                    
                self.vbd_dict[wavelength] = iv_data.vbd

                if debug:
                    print('V BD = ', self.vbd_dict[wavelength])

                self.photodiode_data[wavelength] = np.mean(iv_data.pd_current)
                self.photodiode_data_std[wavelength] = np.std(iv_data.pd_current)

                if debug:
                    plt.plot(iv_data.voltage, iv_data.current)

            elif self.files[i_file].find('dark') != -1:
                
                iv_data = IV(self.path + self.files[i_file])
                if debug:
                    print('dark : ', self.files[i_file])

                iv_data.read_IV_file()
                self.sipm_data['dark'] = interp1d(iv_data.voltage, iv_data.current, kind='cubic')
                self.photodiode_data['dark'] = np.mean(iv_data.pd_current)
                self.photodiode_data_std['dark'] = np.std(iv_data.pd_current)
                
                if np.isnan(Vbd):
                    iv_data.calc_vbd(dv = 0.01, debug = False)
                else:
                    iv_data.vbd = Vbd
                
                self.vbd_dict['dark'] = iv_data.vbd

                if debug:
                    plt.plot(iv_data.voltage, iv_data.current, 'r.-', label='dark')

        if debug:
            plt.yscale('log')
            plt.grid()
            plt.xlim([50., 65])
            plt.title(self.device_name)
            plt.ylim([0.4e-10, 1.e-3])
            plt.xlabel("$V_{bias}$, V")
            plt.ylabel("Current, A")
            plt.legend()
            plt.savefig('results/IV_wavelength.png', dpi=600)
            #plt.show()
