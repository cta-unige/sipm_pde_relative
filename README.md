# sipm_pde_relative

Project to analyse SiPM reverse and forward IV curves. As well as for relative PDE analysis. 


## Description

Project to analyse SiPM reverse and forward IV curves. As well as for relative PDE analysis. 

Before using the project, it is strongly recomended to read article [Characterisation of a large area silicon photomultiplier](https://arxiv.org/pdf/1810.02275.pdf). In particular part Section #5 


## Structure
Project containes:
*   IV.py                              -> calss to display and basic IV analysi;
*   reverse_iv.py                      -> dougter class of IV.py to calcultate relative PDE
*   analysis_script.ipynd              -> jupiter notebook with exapels how to analyse relative PDE data and use IV.py and reverse_iv.py
*   PD_S1337-1010BQ_SN01.out           -> QE for calibrated photodiode, S/N=1, Lot 61, from Dec. 2016 (ID2 photodiode)
*   cal-S1337-1010BQ_SN2.dat           -> QE for calibrated photodiode, Oldest photodiode soldered in PCB (Unige photodiode)
*   s1337-1010bq_spectral_response.dat -> QE for calibrated photodiode, S/N=1, Lot 3F, from Sep. 2013 (Unige photodiode)
*   results                            -> folder with some saved results
*   images                             -> folder with some images used to discribe analysis procedure
*   data_example                       -> filder with experimental data
 
## Usage
To use the project, the txt files with experimental data is neede. Detailed description how to use **reverse_iv** class, as well as neede input data format can be found in jupiter notebook **analysis_script**


## Authors and acknowledgment
Developed by A. Nagai at University of Geneve, DPNC

## License
For open source projects, say how it is licensed.
